module.exports = {
  plugins: [
    `typography-theme-fairy-gates`,
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`,
      },
    },
  ],
}